cmake_minimum_required(VERSION 2.8.3)
project(hark_msgs)

find_package(catkin REQUIRED COMPONENTS geometry_msgs message_generation std_msgs)


add_message_files(
  DIRECTORY msg
  FILES
   HarkFeature.msg
  HarkFeatureVal.msg
  HarkFFT.msg
  HarkFFTVal.msg
  HarkInt.msg
  HarkJulius.msg
  HarkJuliusSrc.msg
  HarkJuliusSrcVal.msg
  HarkSource.msg
  HarkSourceVal.msg 
  HarkSrcFeature.msg
  HarkSrcFeatureMFM.msg
  HarkSrcFeatureMFMVal.msg
  HarkSrcFeatureVal.msg
  HarkSrcFFT.msg
  HarkSrcFFTVal.msg
  HarkSrcWave.msg
  HarkSrcWaveVal.msg
  HarkWave.msg
  HarkWaveVal.msg
)

add_service_files(
  FILES
  HarkIntSrv.srv
)


generate_messages(DEPENDENCIES geometry_msgs std_msgs)

catkin_package(
  CATKIN_DEPENDS geometry_msgs message_runtime std_msgs)

install(DIRECTORY include/${PROJECT_NAME}/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
  FILES_MATCHING PATTERN "*.h")

