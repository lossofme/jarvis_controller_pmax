#pragma once


#include <ros/ros.h>
#include <ros/package.h>


#include "jarvis_body_hand/record_hand_position.hpp"
#include "jarvis_body_hand/kinematic.hpp"
#include "jarvis_body_hand/actuator.hpp"
#include "jarvis_body_hand/control_input.hpp"
#include "jarvis_body_hand/drivebase.hpp"
#include "jarvis_body_hand/voice.hpp"

namespace jarvis_body_hand {

/*!
 * Class containing the algorithmic part of the package.
 */
class Command
{

 private:
  //! Internal variable to hold the current average.
  //! ROS node handle.
  ros::NodeHandle&  nodeHandle_;
  Voice*            voice_;
  ControlInput*     controlin_;
  Kinematic*        kinetic_;
  Actuator*         actuator_;
  Record_hand*      rec_hand_;
  DriveBase*        drivebase_;

  //! ROS topic publisher.

  void Do_list();
  void Do_armsethome();
  void Do_armmove();
  void Do_getwheelvelocity();
  void Do_getarmangle();
  void Do_getheadangle();

  bool STOP_THREAD;
  bool PAUSED;
  bool REQUEST_RESET;


  void readParameters();
 public:
  


  /*!
   * Constructor.
   */
  Command(ros::NodeHandle& nodeHandle, Voice* voice, ControlInput* controlin, Kinematic* kinetic, Actuator* actuator, Record_hand* rec_hand, DriveBase* drivebase);

  void Run();
  bool Stop();
  void RequestReset();

  /*!
   * Destructor.
   */
  virtual ~Command();





  
};

} /* namespace */
