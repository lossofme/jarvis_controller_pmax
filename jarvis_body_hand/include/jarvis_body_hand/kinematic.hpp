#pragma once
/*
 * Description : Modular arm inverse kinematic handler framework
 * Author      : Thanabadee Bulunseechart
 */
#include <vector>
#include <Eigen/Dense>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>


// #include <moveit/move_group_interface/move_group.h> #this for indigo please use class MoveGroup instead MoveGroupInterface
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

#include <moveit_visual_tools/moveit_visual_tools.h> // simple tool for showing grasps


#include <control_msgs/FollowJointTrajectoryFeedback.h>

#include <ros/ros.h>

#include "jarvis_body_hand/voice.hpp"

#include <std_srvs/Empty.h>


// Grasp generation
// #include <moveit_simple_grasps/simple_grasps.h>

// Baxter specific properties
// #include <moveit_simple_grasps/custom_environment2.h>


namespace jarvis_body_hand {


/*!
 * Class containing the kinematic (forward and inverse) part of the package.
 */
class Kinematic
{
 public:
  /*!
   * Constructor.
   */
  Kinematic(ros::NodeHandle& nodeHandle, const std::string &move_group, const std::string &eef, Voice* voice);

  /*!
   * Destructor.
   */
  virtual ~Kinematic();


  /*!
   *  Change move group and end-effector.
   */
  void SetMoveGroup(const std::string &move_group, const std::string &eef);

  /*!
   *  Set current state of robot arm.
   */
  void setJointAngleModel(const control_msgs::FollowJointTrajectoryFeedback::ConstPtr& joint_states_);
  void setJointAngleModel(const sensor_msgs::JointState::ConstPtr &joint_states_);

  /*!
   *  Get 4x4 Matrix transfomation of end-effector.
   */
  void CurrentPose(Eigen::Affine3d &end_effector_state);

  /*!
   *  Under construct (Forward kinematic of current joint in real robot)
   */
  bool getPos(std::vector<double> &trans,std::vector<double>&rot);

  /*!
   *  Convert position-orientation to joint desired of simulation robot
   */
  bool pToJ(Eigen::Vector3d trans ,Eigen::Vector3d rot);
  bool MoveArmTo(Eigen::Vector3d trans ,Eigen::Vector3d rot);
  bool MoveArmTo(Eigen::Vector3d trans, Eigen::Vector3d rot, bool wrist_up);
  bool setHome();
  /*!
   *  Debug mode.. Just shout some step out for now.
   */
  void Debug(bool Debug);

  void ResetOctomap();

  /*!
   *  Get Grippers state
   */
  double GetGripState();

  bool SetGripperOpen(bool OPEN);
  bool GrapAt(Eigen::Vector3d trans ,Eigen::Vector3d rot);
  bool PlaceAt(Eigen::Vector3d trans ,Eigen::Vector3d rot);



  void AddBox(Eigen::Vector3d trans, Eigen::Vector3d rot, Eigen::Vector3d size, std::string NameID);
  void RemoveBox(std::string NameID);
  void AddObj(Eigen::Vector3d trans, Eigen::Vector3d rot, Eigen::Vector3d size, std::string NameID);
  /*!
    * (OUTDATE) Get position 3D from joint angle the forward kinematic
    */
  void getForwardK(double In[5],double Out[3]) const;
  /*!
    * (OUTDATE) Get joint angle from xyz the inverse kinematic
    */
  void getInverseK(double In[3],double Out[5]) const;


  bool arm_has_object = false;

 private:

  ros::NodeHandle& nodeHandle_;
  ros::Publisher planning_scene_diff_publisher;
  
  //Parameters
  std::vector<int> JOINT_RVIZ;
  int JOINT_N; 
  double GRIP_CLOSE;
  double GRIP_OPEN;

  bool readParameters();

  Voice* voice_;


  
  robot_model_loader::RobotModelLoader robot_model_loader;
  robot_model::RobotModelPtr kinematic_model;
  robot_state::RobotStatePtr kinematic_state;
  const robot_state::JointModelGroup* joint_model_group;
  std::vector<std::string> joint_names;

  moveit::planning_interface::MoveGroupInterfacePtr group;
  moveit::planning_interface::MoveGroupInterfacePtr gripper_group;
  moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
  moveit::planning_interface::MoveGroupInterface::Plan my_plan;

  std::vector<double> joint_values;
  std::vector<double> joint_targets;
  control_msgs::FollowJointTrajectoryFeedback::ConstPtr joint_states_;
  bool joint_states_updated;

  //! Assume 1 chain link has 1 gripper
  double grippers_state;



  std::string movegroup_, eef_;
  bool DEBUG;
  

  //! Internal variable to hold the current average.
  double average_;

  //! Number of measurements taken.
  unsigned int nMeasurements_;





planning_scene_monitor::PlanningSceneMonitorPtr planning_scene_monitor_;
//     // Grasp generator
//   moveit_simple_grasps::SimpleGraspsPtr simple_grasps_;

//   // class for publishing stuff to rviz
//   moveit_visual_tools::MoveItVisualToolsPtr visual_tools_;

//   // robot-specific data for generating grasps
//   moveit_simple_grasps::GraspData grasp_data_;




  ros::Publisher object_to_display;
  moveit_msgs::AttachedCollisionObject attached_object;


   ros::ServiceClient clear_octomap;


};

} /* namespace */
