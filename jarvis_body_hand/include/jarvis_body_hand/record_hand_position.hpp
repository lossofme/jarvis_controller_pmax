#pragma once
/*
 * Description : Record joints position and playback
 * Author      : Thanabadee Bulunseechart
 */
#include "jarvis_body_hand/actuator.hpp"


#include <dynamixel_msgs/MotorStateList.h>
#include <dynamixel_msgs/MotorState.h>
#include <control_msgs/FollowJointTrajectoryFeedback.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <dynamixel_msgs/JointState.h>
#include <std_msgs/Float64.h>

#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <ros/ros.h>
#include <ros/package.h>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH
#include <queue>


#include <iomanip>
#include <chrono>
#include <ctime>


namespace jarvis_body_hand {

/*!
 * Class containing the algorithmic part of the package.
 */
class Record_hand
{

 private:
  //! Internal variable to hold the current average.
  //! ROS node handle.
  ros::NodeHandle& nodeHandle_;


  //! Number of measurements taken.
  unsigned int nMeasurements_;
  std::clock_t startcputime;
  bool STOP_THREAD;
  bool PAUSED;
  bool REQUEST_RESET;

  std::vector<dynamixel_msgs::MotorStateList::ConstPtr> MotorListMsgQueue;



  void readParameters();

  Actuator* actuator_;
 public:
  
  bool updated;


  trajectory_msgs::JointTrajectoryPoint actual;
  /*!
   * Constructor.
   */
  Record_hand(ros::NodeHandle& nodeHandle, Actuator* actuator);

  void LoadBag( std::string bag_name );
  void Run();
  bool Stop();
  void RequestReset();
  bool FINISH() const;
  /*!
   * Destructor.
   */
  virtual ~Record_hand();

  /*!
   * Add new measurement data.
   * @param data the new data.
   */
  void addState(const dynamixel_msgs::MotorStateList::ConstPtr& motorstates);
  void addState(const control_msgs::FollowJointTrajectoryFeedback::ConstPtr& motorstates);
  void addState(const dynamixel_msgs::JointState::ConstPtr& motorstates);




  
};

} /* namespace */
