#pragma once


#include <ros/ros.h>
#include <ros/package.h>


namespace jarvis_body_hand {

/*!
 * Class containing the algorithmic part of the package.
 */
class Template
{

 private:
  //! Internal variable to hold the current average.
  //! ROS node handle.
  ros::NodeHandle& nodeHandle_;
  //! ROS topic publisher.



  bool STOP_THREAD;
  bool PAUSED;
  bool REQUEST_RESET;


  void readParameters();
 public:
  


  /*!
   * Constructor.
   */
  Template(ros::NodeHandle& nodeHandle);

  void Run();
  bool Stop();
  void RequestReset();

  /*!
   * Destructor.
   */
  virtual ~Template();





  
};

} /* namespace */
