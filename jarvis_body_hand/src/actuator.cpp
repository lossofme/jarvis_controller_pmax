#include "jarvis_body_hand/actuator.hpp"
/*
 * Description : Dynamixel motors handler and publish its tf
 * Author      : Thanabadee Bulunseechart
 */


namespace jarvis_body_hand {

Actuator::Actuator(ros::NodeHandle& nodeHandle, ControlInput* controlin, Voice* voice)
    : nodeHandle_(nodeHandle),
      controlin_ (controlin),
      voice_(voice)
{
	//Thread prepare
	STOP_THREAD = false;
	REQUEST_RESET = true;
	PAUSED=false;

	//nessesary variable
	JOINT_STATES_UPDATED_ = false;

	readParameters();

	subMotAll_ = nodeHandle_.subscribe("/f_arm_controller/state", 1,
                                      &Actuator::AllMotorCallback, this);

	#ifdef USE_SOUND_LOCALIZATION
		subSoundLoc_ = nodeHandle_.subscribe("/hark_source", 5, &Actuator::SoundLocCallback, this);
	#endif

	//INITIALIZE VARIABLE
	for(uint8_t j=0; j<JOINT_GROUP.size(); j++)
	    for(uint8_t i =0; i < JOINT_N; i++)
	      pubMot.push_back(nodeHandle_.advertise<std_msgs::Float64>
	      	("/arm" + std::to_string(JOINT_START_IDX[j]+i) + "_controller/command", 10));

	for(uint8_t i=0;i<HEAD_IDX.size();i++)
		pubMot.push_back(nodeHandle_.advertise<std_msgs::Float64>
	      	("/head" + std::to_string(i) + "_controller/command", 10));

	for(uint8_t j=0; j<4; j++)
		pubMot.push_back(nodeHandle_.advertise<std_msgs::Float64>
			("/wheel" + std::to_string(j) + "_controller/command", 10));

	ROS_INFO("Actuator: Successful launch");



}

Actuator::~Actuator()
{
	delete voice_;
	delete controlin_;
}


void Actuator::TF_head_process()
{
	
	head_joint_pos.clear();
	head_joint_goal.clear();
	for (uint8_t i = 0; i < HEAD_IDX.size(); ++i)
	{
		head_joint_pos.push_back(joint_states_->actual.positions[HEAD_IDX[i]]);
		head_joint_goal.push_back(joint_states_->desired.positions[HEAD_IDX[i]]);
	}

	//ADD MORE TRANSFORM RELATION HERE
	static tf::TransformBroadcaster head_pan_br, 
									head_tilt_br;
  	tf::Transform transform;
  	tf::Quaternion q;
  	q.setRPY(0,0,head_joint_pos[0]);
  	transform.setOrigin(tf::Vector3(0.01,0,1.32));
    transform.setRotation(q);

  	head_pan_br.sendTransform(
  		tf::StampedTransform(
  			transform, ros::Time::now(), "base_link", "head_pan_link"));
  	q.setRPY(0,-head_joint_pos[1],0);
  	transform.setOrigin(tf::Vector3(0,0,0));
    transform.setRotation(q);

  	head_tilt_br.sendTransform(
  		tf::StampedTransform(
  			transform, ros::Time::now(), "head_pan_link", "head_tilt_link"));
}
void Actuator::setObjectOfInterest(const std::string& object_name)
{
	object_name_ = object_name;
}

void Actuator::setPersonOfInterest(const std::string& object_name)
{
	person_name_ = object_name;
}
bool Actuator::LookingForPerson()
{
	bool success;
	static int score = 0;
	tf::StampedTransform transform;
	try{
		listener.lookupTransform("/camera_rgb_optical_frame", 
					 std::string(person_name_), 
					 ros::Time(0), transform);

		tf::Vector3 relative_pose;
		relative_pose = transform.getOrigin();

		//Take care about lastest transform, some data is the same 
		if(ros::Time::now() - transform.stamp_ < ros::Duration(1.0) )
			++score;
		else
			--score;
		

		if(score>=5)
		{
			PIDTrackingRelativePose(relative_pose);
			success= true;
		}else{
			success= false;
		}
		
		
		
	}catch (tf::TransformException &ex) {
		  --score;		  
		  success= false;
	}
	score = constrain(score, 0, 10);

	// ROS_INFO("score = %d", score);
	return success;
}
bool Actuator::GetPersonGoal(tf::Vector3& persongoal_out)
{
	tf::StampedTransform transform;
	try{
		listener.lookupTransform("/map", 
					 std::string(person_name_), 
					 ros::Time(0), transform);
		if(ros::Time::now() - transform.stamp_ < ros::Duration(1.0))
		{
			persongoal_out = transform.getOrigin();
			ROS_INFO_STREAM(persongoal_out);	
			return true;
		}
		else{
			return false;
		}

		ROS_INFO_STREAM_THROTTLE(20, "Actuator : LookingForPerson");
		
	}catch (tf::TransformException &ex) {

		  // ROS_ERROR("%s",ex.what());
		  // ros::Duration(0.05).sleep();
		  // // continue;
		return false;
	}

}

bool Actuator::LookingFor(const std::string& object_name, tf::StampedTransform& Stransform)
{
	//FIX ME !!
	object_name_ = object_name;
	try{
		listener.lookupTransform("/base_link", object_name, ros::Time(0), Stransform);
		// ROS_INFO_STREAM("I get x = " << Stransform.getOrigin().x() << "\t" << Stransform.getOrigin().y());
		// ROS_INFO_STREAM("GET ROS TIME\t" << ros::Time::now() << "\t" << Stransform.stamp_);
		return true;
	}
	catch (tf::TransformException &ex) {
	  // ROS_ERROR("%s",ex.what());
	  ros::Duration(0.05).sleep();
	  // continue;
	  return false;
	}
}
int Actuator::GetObjectIdx(const std::string& object_name)
{
	//Get idx of given name
	for(int i=0; i<OBJECT_NAME.size(); i++)
	{
		if(OBJECT_NAME[i]==object_name)
			return i;
	}

	return -1;
}

void Actuator::PIDTrackingRelativePose(tf::Vector3 relative_pose)
{
	// ROS_INFO_STREAM("RPOS: " << relative_pose.x() 
	// 				 << "\t" << relative_pose.y() 
	// 				 << "\t" << relative_pose.z());
	if( relative_pose.z() !=0 )
	{
		double error_pan = -atan(relative_pose.x()/relative_pose.z());
		double error_tilt= -atan(relative_pose.y()/relative_pose.z());
		std::vector<double> head;
		double theta_pan = joint_states_->actual.positions[HEAD_IDX[0]]
							 + 0.6*error_pan;	//pan
		double theta_tilt= joint_states_->actual.positions[HEAD_IDX[1]]
							 + 0.3*error_tilt;
		theta_pan 	= constrain(theta_pan,-2.0, 2.0);


		theta_tilt 	= constrain(theta_tilt, -1.0471, 0.523); //+=30deg -> -60deg
		head.push_back(theta_pan);
		head.push_back(theta_tilt);

		if(HEAD_CONTROL_MODE == HEAD_FOLLOW_PERSON)
			head[1] = 0.0;

		setHead(head);
		ROS_INFO_STREAM_THROTTLE(20,"SET HEAD: " << error_pan
					 		 << "\t" << error_tilt);
	}
}
bool Actuator::TrackingFor(const std::string& object_name, tf::StampedTransform& Stransform)
{



	//Get object idx for searching
	int idx = GetObjectIdx(object_name);
	int idx_next;
	if(idx ==-1)
	{
		ROS_ERROR("Actuator: OBJECT NAME YOU GIVEN IS NOT IN LIST!");

	}else{
		
		uint16_t id_start = OBJECT_ID[idx];
		uint16_t id_end;
		if(idx!=OBJECT_NAME.size()-1)	id_end = OBJECT_ID[idx+1]-1;
		else						    id_end = OBJECT_LAST_ID;
		
		ROS_INFO_STREAM("IDX:"<<idx<<id_start<<"\t"<<id_end);
		std::vector<tf::StampedTransform> vStransform;
		ros::Time lastest_transform;
		int lastest_transform_id = -1;
		
		for(uint8_t j=0; j<=id_end-id_start; j++)
		{
			tf::StampedTransform transform;
			try{
				listener.lookupTransform("/camera_rgb_optical_frame", 
							 std::string("/object_") + std::to_string(id_start+j), 
							 ros::Time(0), transform);
				
			}catch (tf::TransformException &ex) {
				  // ROS_ERROR("%s",ex.what());
				  // ros::Duration(0.05).sleep();
				  // // continue;
				  // return false;
			}
			vStransform.push_back(transform);
			// ROS_INFO_STREAM("TEST" << j << ros::Time::now() << "\t" << transform.stamp_ << "\tsize" << vStransform.size());

			//Get lastest transform and id for used to localization object
			if(ros::Time::now() - transform.stamp_ < ros::Duration(2) &&
			   transform.stamp_ > lastest_transform)
			{

				lastest_transform = transform.stamp_;
				lastest_transform_id = j;
				Stransform = transform;
			}
				
		}
		
		if(lastest_transform_id	== -1)
		{
			ROS_ERROR("Actuator: Can't get lastest transform");
			return false;
		}else{
			ROS_INFO_STREAM("Actuator: Tracking object -> " << OBJECT_NAME[idx]);
			tf::Vector3 relative_pose;
			relative_pose = vStransform[lastest_transform_id].getOrigin();
			PIDTrackingRelativePose(relative_pose);
			
			// tf::Vector3 rpy;
			// tf::Matrix3x3(Stransform.getRotation()).getRPY(rpy[0], rpy[1], rpy[2]);
			// ROS_INFO_STREAM("RPY: " << rpy.x() << "\t" << rpy.y() << "\t" << rpy.z());
			return true;
		}
	}
	
}





void Actuator::SearchingForSometing(ros::Time last_stamp)
{
	if(ros::Time::now() - last_stamp > ros::Duration(2.0) )
	{

		static double Head[2] = {0,0};
		static double DIR = 1.0;

		if(head_joint_pos[0] > 2.0)
		{
			DIR=-1;
		}else if(head_joint_pos[0] < -2.0)
		{
			DIR=1;
		}
		Head[0] +=  DIR*HEAD_SEARCHING_SPEED;
		setHead({Head[0], 0.0});

	}
}
void Actuator::Run() 
{
	ros::Rate r(10);
	while(ros::ok())
	{
		//TODO check empty file , etc
		if( !STOP_THREAD ) 
		{
			//GET HEAD JOINT VALUE
			if(JOINT_STATES_UPDATED_) //prevent non-member call case 
			{
				//Process head tf tranformation to link between head and camera
				TF_head_process();

				//Check if dynamixel not update. We can't use at all.
				if(head_joint_goal.size()==0 || head_joint_pos.size()==0) continue;

				HEAD_CONTROL_MODE = controlin_->GetHeadMode();
				tf::StampedTransform Stransform;

				if(HEAD_CONTROL_MODE==HEAD_MANUAL)
				{
					//Init head position
					static double Head[2] = {0,-0.3};
					double HeadVel[2];
					controlin_->GetHeadControl(HeadVel);

					Head[0] += HeadVel[0];
					Head[1] += HeadVel[1];
					setHead({Head[0], Head[1]});
					ROS_INFO_THROTTLE(20,"Actuator: Cur head position %.2f -- %.2f", Head[0], Head[1]);

				}

				else if(HEAD_CONTROL_MODE==HEAD_FOLLOW_OBJECT)
				{
					if(object_name_.length()==0)
					{
						ROS_ERROR_THROTTLE(20,"Actuator: Can't detech object name of interest!");
					}
					else
					{
						bool found = TrackingFor(object_name_, Stransform);
					}
				}

				else if(HEAD_CONTROL_MODE==HEAD_FOLLOW_PERSON)
				{
					static ros::Time last_stamp = ros::Time::now();
					if(!LookingForPerson())
					{
						SearchingForSometing(last_stamp);
					}else{
						last_stamp = ros::Time::now();
					}
				}

				

			  	JOINT_STATES_UPDATED_ = false;

			}
			
			// ROS_INFO("ACTUATOR_RUNNING_BITCH!");
			//Safe area to stop
			if(STOP_THREAD)
			{
				// break;
				while(STOP_THREAD && ros::ok()) {
					PAUSED = true;
					r.sleep();
				}
			}
			PAUSED=false;
		}else
		{
			PAUSED = true;
		}
		r.sleep();
	}
}

bool Actuator::Stop() 
{
	STOP_THREAD=true;
	return PAUSED;
}
void Actuator::RequestReset() {
	REQUEST_RESET = true;
	STOP_THREAD = false;
	
}

void Actuator::setRawMot(const sensor_msgs::JointState::ConstPtr& message)
{
	for(uint8_t j =0; j< JOINT_GROUP.size(); j++ )
	    for(uint8_t i =0; i< JOINT_N; i++)
	    {
	      uint8_t idx = JOINT_START_IDX[j] + i;
	      if(i!=JOINT_N-1)
	      {
	      	std_msgs::Float64 mot;
	      	mot.data = (double)JOINT_IK_DIR[ idx ] * message->position[ (int)JOINT_RVIZ[ idx ] ];
	        pubMot[idx].publish(mot ); 
	      }
	    }
}

void Actuator::setRawMotALL(const dynamixel_msgs::MotorStateList::ConstPtr& motorstates)
{

	//TODO(MAX): set priority to interrupt it if nessesary
	for(uint8_t j =0; j< JOINT_GROUP.size(); j++ )
		for(uint8_t i =0; i< JOINT_N; i++)
		{
			uint8_t idx = JOINT_START_IDX[j] + i;
			std_msgs::Float64 mot;
			mot.data = (motorstates->motor_states[idx].position
										-JOINT_RAW_HOME[idx]) 
											*JOINT_SCALE[idx] ;
			pubMot[idx].publish(mot);
		}
	  
}

void Actuator::setGripper(std::vector<double> gripper_state)
{
	for(uint8_t j =0; j< JOINT_GROUP.size(); j++ )
	    for(uint8_t i =0; i< JOINT_N; i++)
	    {
	      uint8_t idx = JOINT_START_IDX[j] + i;
	      if(i == JOINT_N-1)
	      {
	      	std_msgs::Float64 mot;
		  	mot.data = gripper_state[j];
	        pubMot[idx].publish( mot );
	      }
	    }
}


void Actuator::setHead(std::vector<double> head)
{
	if(head.size() != HEAD_IDX.size()) {
		ROS_ERROR_THROTTLE(2, "Actuator: topic to head joint and it index mismatched");
		return;
	}
	for(uint8_t i=0;i<HEAD_IDX.size();i++)
	{
		std_msgs::Float64 mot;
		mot.data = head[i];
		pubMot[HEAD_IDX[i]].publish(mot);
	}
}

void Actuator::setWheel(std::vector<double> wheel)
{
	for(uint8_t i=0; i<WHEEL_IDX.size(); i++)
	{
		std_msgs::Float64 WheelMsgs;
		WheelMsgs.data = wheel[i];
		pubMot[WHEEL_IDX[i]].publish(WheelMsgs);
	}
}

void Actuator::getHead(std::vector<double> &headOut)
{
	if(JOINT_STATES_UPDATED_)
	for(uint8_t i=0;i<HEAD_IDX.size();i++)
	{
		headOut.push_back(joint_states_->actual.positions[HEAD_IDX[i]]);
	}
}
void Actuator::getWheel(std::vector<double> &wheelOut)
{
	if(JOINT_STATES_UPDATED_)
	for(uint8_t i = 0; i < WHEEL_IDX.size(); i++)
	{
		wheelOut.push_back(joint_states_->actual.velocities[WHEEL_IDX[i]]);
	}
}
void Actuator::getArm(std::vector<double> &armOut)
{
	if(JOINT_STATES_UPDATED_)
	for(uint8_t j=0; j<JOINT_GROUP.size(); j++)
    for(uint8_t i =0; i < JOINT_N; i++)
	{
		armOut.push_back(joint_states_->actual.positions[JOINT_START_IDX[j]+i]);
	}
}

void Actuator::AllMotorCallback(const control_msgs::FollowJointTrajectoryFeedback::ConstPtr &message)
{
  joint_states_ = message;
  JOINT_STATES_UPDATED_ = true;
}


void Actuator::readParameters()
{
	nodeHandle_.getParam("/JOINT_N", JOINT_N);
	nodeHandle_.getParam("/JOINT_SCALE", JOINT_SCALE);
	nodeHandle_.getParam("/JOINT_RAW_HOME", JOINT_RAW_HOME);
	nodeHandle_.getParam("/JOINT_START_IDX", JOINT_START_IDX);
	nodeHandle_.getParam("/JOINT_GROUP", JOINT_GROUP);
	nodeHandle_.getParam("/JOINT_RVIZ", JOINT_RVIZ);
	nodeHandle_.getParam("/JOINT_IK_DIR", JOINT_IK_DIR);
	nodeHandle_.getParam("/HEAD_IDX", HEAD_IDX);
	nodeHandle_.getParam("/WHEEL_IDX", WHEEL_IDX);
	nodeHandle_.getParam("/OBJECT_LAST_ID", OBJECT_LAST_ID);
	nodeHandle_.getParam("/OBJECT_ID", OBJECT_ID);
	nodeHandle_.getParam("/OBJECT_NAME", OBJECT_NAME);

	nodeHandle_.getParam("/HEAD_SEARCHING_SPEED", HEAD_SEARCHING_SPEED);

	if(JOINT_RAW_HOME.size() != JOINT_SCALE.size()) 
	{
		ROS_ERROR("Actuator: Could not read parameters. Check size of parameters. Please use roslaunch to include param.");
    	ros::requestShutdown();
		while(ros::ok());
	}
}










#ifdef USE_SOUND_LOCALIZATION

	void Actuator::SoundLocCallback(const hark_msgs::HarkSource::ConstPtr& msg)
	{
		ROS_INFO("hark_msgs is received");
	}
#endif


} /* namespace */
